const axios = require("axios");
const admin = require("firebase-admin");
const serviceAccount = require("./at202xcloud-e8a6266f9a57.json");
admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });
const db = admin.firestore();

exports.run = (req, res) => {
  //  {
  //    "order": {
  //      "units" : "420",
  //      "stopLossOnFill": {
  //        "timeInForce": "GTC",
  //        "price": "1.2280"
  //      },
  //      "takeProfitOnFill": {
  //        "price": "1.2325"
  //      },
  //      "timeInForce": "FOK",
  //      "instrument": "EUR_USD",
  //      "type": "MARKET",
  //      "positionFill": "DEFAULT"
  //    }}

  // const bodyCompare = {
  //   order: {
  //     units: "420",
  //     stopLossOnFill: {
  //       timeInForce: "GTC",
  //       price: "1.2120",
  //     },
  //     takeProfitOnFill: {
  //       price: "1.2260",
  //     },
  //     timeInForce: "FOK",
  //     instrument: "EUR_USD",
  //     type: "MARKET",
  //     positionFill: "DEFAULT",
  //   },
  // };
  const payload = req.body
  console.log(payload, "PAYLOAD")
  const options = {
    method: "post",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
    },
  };
  // // if (message == typeof(Object)) {
  console.log("before Axios")
  const body = {
    order: {
      units: payload.units,
      stopLossOnFill: {
        timeInForce: "GTC",
        price: payload.stopLoss,
      },
      takeProfitOnFill: {
        price: payload.takeProfit,
      },
      timeInForce: "FOK",
      instrument: payload.instrument,
      type: "MARKET",
      positionFill: "DEFAULT",
    },
  };
  axios
    .post(
      "https://api-fxpractice.oanda.com/v3/accounts/101-004-10015616-001/orders",
      body,
      options
    )

    .then(function (response) {
      // let tradeId = await response.data.orderFillTransaction.id
      res.send(response.data.orderFillTransaction.id);
    })
    .catch(function (error) {
      // handle error
      console.log(error.message);
    });
  // }
  // else {
  //   _res.send("Request has been send without parameters")
  // }
};

